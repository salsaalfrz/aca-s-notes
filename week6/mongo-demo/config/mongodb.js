const { MongoClient } = require("mongodb");
let database = null;

function getDatabase () {
  return database //! ambil nilai setelah function nya diinvoke
}

//! diexports untuk di pass ke app.js
async function connect() {
  try {
    const uri ="mongodb://localhost:27017";
    const client = new MongoClient(uri,
      {useUnifiedTopology:true, useNewUrlParser: true});

    await client.connect();
    // ! membuat database baru / yg sudah ada 
    const db = client.db('ib16');
    database = db
    return db;
  } catch(error) {
    // Ensures that the client will close when you finish/error
    console.log(error);
  }
}


module.exports = {
  connect,
  getDatabase
};