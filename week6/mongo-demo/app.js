const express = require('express');
const app = express();
const PORT = process.env.PORT || 5000;
// ! ambil dari cofiguration yg dibuat di folder config/mongodb
const {connect, database} = require('./config/mongodb')
const userRouter = require('./routes/userRouter')

// ! menjalankan database mongodb


app.use(express.json())
app.use(express.urlencoded({ extended : true}))
app.get('/',async (req,res) => {
  try {
    const users = await database.collection('ib16').find().toArray();
    res.json(users)
  } catch (error) {
    console.log(error);
  }
})



app.use('/users', userRouter);

connect()
  .then(async (db) => {
    console.log(`mongodb berhasil connect!`);
    const users = await db.collection('ib16').find().toArray;
    console.log(users,`<< ini dari users`);
    app.listen(PORT, () => {console.log(`mongo berjalan di port ${PORT}`)})
  })