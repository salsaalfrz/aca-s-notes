const express = require('express');
const router = express.Router();
// ! panggil controller
const UserController = require('../controllers/UserController');

router.get('/', UserController.findAll);


module.exports = router;