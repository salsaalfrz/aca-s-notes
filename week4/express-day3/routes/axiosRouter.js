const express = require('express');
const router = express.Router();
const axios = require('axios');

router.get('/api', (req,res) => {
  // console.log(`ini dari axios`);
  return axios
    .get('https://cat-fact.herokuapp.com/facts')
    .then((data) => {
      res.send(data.data)
    })
    .catch((err) => {
      console.log(err.message);
    })
})

router.get('/fetch', (req,res) => {
  fetch(' https://cat-fact.herokuapp.com')
  .then(data => res.send(data.data))
  .catch(err => console.log(err));
})


module.exports = router;