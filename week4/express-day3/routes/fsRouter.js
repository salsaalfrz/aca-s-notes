const express = require("express");
const router = express.Router();
//! is a must
const fs = require("fs");

router.post("/:fileName", (req, res) => {
  const {data} = req.body;
  const { fileName } = req.params;
  fs.writeFile(`temp/${fileName}.json`, JSON.stringify(data), (err) => {
    if (err) {
      return res.status(500).json({
        status: "failed",
        message: "failed to create file",
        code: 500,
        result: data,
      });
    }
    return res.status(201).json({
      message: "file created successfuly",
      status: "success",
      code: 201,
      result: data,
    });
  });
});

router.get("/:fileName", (req, res) => {
  const { fileName } = req.params;
  fs.readFile(`temp/${fileName}.json`, { encoding: "utf-8" }, (err, data) => {
    if (err) {
      return res.status(500).json({
        status: "failed",
        message: err.message,
        code: 500,
        result: data
      });
    }
    console.log("satu");
    return res.status(200).json({
      status: "success",
      code: 200,
      message: "Successfully retrieve the data",
      result: JSON.parse(data),
    });
  });
  console.log("dua");
});

router.put("/:fileName", (req, res) => {
  const body = req.body;
  fs.readFile(`temp/${req.params.fileName}.json`, (err, data) => {
    if (err) {
      return res.status(404).json({
        status: "failed",
        message: "no such file in directory",
        code: 404,
        result: data
      });
    }
    fs.writeFile(`temp/${req.params.fileName}.json`, JSON.stringify(body), (err) => {
      if (err) {
        return res.status(500).json({
          status: "failed",
          message: err.message,
          code: 500,
          result: data
        });
      }
      return res.status(200).json({
        status: "success",
        message: "successfully update the file",
        code: 200,
        result: data,
      });
    });
  });
});

router.delete("/:fileName", (req, res) => {
  fs.unlink(`temp/${req.params.fileName}.json`, (err) => {
    if (err) {
      return res.status(404).json({
        status: "failed",
        message: "file not found",
        code: 404,
        result: {},
      });
    }
    return res.status(200).json({
      status: "success",
      message: "file deleted successfully",
      code: 200,
      result: {},
    });
  });
});

router.patch("/:before/:after", (req, res) => {
  const { before, after } = req.params;
  fs.rename(`temp/${before}.json`, `temp/${after}.json`, (err) => {
    if (err) {
      return res.status(500).json({
        status: "failed",
        code: 500,
        result: {},
        message: err.message,
      });
    }
    return res.status(200).json({
      status: "success",
      code: 200,
      result: {},
      message: "Successfully rename the file",
    });
  });
});

module.exports = router;
