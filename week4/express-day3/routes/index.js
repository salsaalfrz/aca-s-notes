const express = require('express')
// const app = express()
// const router = app.Router()
const router = express.Router();
const fsRouter = require('./fsRouter')
const axiosRouter = require('./axiosRouter')
//!         endopoint   file yg udah di import
router.use("/fs", fsRouter) // prefix
router.use('/api', axiosRouter)

module.exports = router;