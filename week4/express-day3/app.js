const express = require("express");
//! by default -> ketika import file akan menuju ke index
const router = require('./routes') // mengimport apa yang diexport dari file tersebut
const app = express();
const port = 5000;

app.use(express.json());
//! untuk allow router terbaca
app.use(router)

app.listen(port, () => {
  console.log(`Server started on port`, port);
});


// router (modem)
// extender