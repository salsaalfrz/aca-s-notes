[**Back to Home**](./../README.md)

# Prosedur Pengumpulan Tugas

1.  `git clone <alamat-repo-tugas>` untuk mengunduh file tugas.
2.  `cd <folder-repo>` untuk masuk ke folder tugas.
3.  `code .` untuk membuka code editor (Visual Studio Code).
4.  `git checkout -b <nama-kalian>` untuk membuat branch yang nantinya akan kalian submit.
5.  `node <nama-file>` untuk mengeksekusi kode program yang kalian buat.
6.  `git add .` untuk memasukkan perubahan-perubahan yang kalian lakukan ke staging area.
7. `git commit -m "<pesan-commit>"` untuk menyimpan perubahan-perubahan yang kalian lakukan ke repository local kalian.
8. `git push origin <nama-branch>` untuk melakukan update ke remote repository, dalam hal ini GitHub.
9. Buat pull request di GitHub untuk branch kalian. Title pull request-nya diisi dengan nama kalian.

[**Back to Home**](./../README.md)
