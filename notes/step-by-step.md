- npm init -y 
- npm i express mysql2 dotenv
- npm i -D sequelize-cli
- don't forget to add .gitignore for node_modules and .env
- mkdir for middlewares (auth -> authenticate , authorize), controllers, routes (routing) and helpers
- make a file app.js
- npm i jsonwebtoken (generate and verify) bcrypt (hashing pass)
- first setting your config.json -> development
- set the uname and password for db 
- activate your xampp / mysql
- helpers function -> make a generate token and verify also hash the pass
- generate the model and the entity (must be capital!) and import the hashing password from bcrypt.js
- add a association among the model also add a validation 
- if the model have a relation -> migrate a file to add a column for foreign key / pivot table (many to many)
- make a routes for the end point
- manipulate the model (give a logic) on controller (don't forget the module.exports!)

tips!
1. import the file,package and library with -> const { pack1 } = require('../models'); 
2. before you use the routes -> res.send first for checking the express if its run smoothly
3. on controller -> import the model that u need to manipulate