/**
 * CAPITAL CASE
 * ============
 *
 * Sebuah fungsi bernama capitalCase akan menerima satu buah parameter yang berupa array of string.
 * Fungsi ini akan mengembalikan array of string yang dimana setiap huruf pertama pada element-nya
 * akan berubah menjadi huruf besar.
 *
 * Example:
 *   - Input  : ['ucing', 'icing', 'acil']
 *   - Output : ['Ucing', 'Icing', 'Acil']
 *
 * RULES :
 *   - Wajib menyertakan pseudocode dan kode program.
 *   - Kerjakan tanpa built-in function kecuali .push(), Number(), String(), .toString(), .toLowerCase(), .toUpperCase().
 */

/**
 * ALGORITMA 
 * =========
 * 
 */
// console.log([] === []);
// function* capitalCase (arr) {
 
//   for (let i = 0; i < arr.length ; i++) {
//     let temp = "";
//     // console.log(arr[i]);
//     let awal = arr[i];
//     for (let j = 0; j < awal.length; j++) {
//       // console.log(awal[j]);
//       if (j === 0) {
//         temp += awal[j].toUpperCase()
//       } else {
//         temp += awal[j] 
//       }
//     }
//     yield temp
//   }
 
// }


//pake spread
// console.log([...capitalCase(['ucing', 'icing', 'acil'])]);

class Student {
  
  data = 1;


  acagj () {

    const arrow = () =>  console.log(this) 

    const fn = function() {
      console.log(this); 
  }

  arrow();
  fn();
  console.log(this);
}
}

(new Student).acagj();