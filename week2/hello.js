// console.log("Hello World")

// ! Function Scope
// let temp = "hehehe"

// function happy() {
//   let temp = "temp is defined in funct scope";
//   console.log(temp);
// }

// happy();
// console.log(temp);

// ! Block Scope

// if (true) {
//   var num1 = 15;
//   let num2 = 25;
//   num1 = 20;
//   console.log(num1);
//   console.log(num2);
// }

// console.log(num1);
// console.log(num2);

// let ucing = "gemoi"
// var ucing = 10

// console.log(ucing)

let name = "Ucing";
//* ini penamaan variable ucing berisi string

// ! hehehe
// ? lucu kan 

/**
 * multi line 
 * algoritma -> bilangan prima 
 */

for (let i = 0; i < 10; i++) {
  console.log("*");
}

/*
ALGORITMA
=========
1. Siapkan pernyataan yang diketahui disoal
2. Melakukan pencocokan atau pemeriksaan pada soal yang tersedia
3. Simpan 'Masa Benda' dengan nilai 600 yang mempunyai satuan kg
4. Simpan 'Percepatan Benda' dengan nilai 2 yang mempunyai satuan meter per sekon kuadrat
5. Simpan 'Resultan Gaya' tanpa nilai. Nanti akan diisi
6. Hitung hasil perkalian dari 'Massa Benda' dengan 'Percepatan Benda'
7. Setelah mendapat hasil perhitungan tersebut. Maka, masukkan atau sisipkan hasil ke dalam string 'Resultan Gaya'
8. Tampilkan nilai dari 'Resultan Gaya'
PSEUDOCODE
==========
STORE massa AS NUMBER WITH 600
STORE percepatan AS NUMBER WITH 2
STORE resultanGaya AS NUMBER WITHOUT ANY VALUE
SET resultanGaya WITH massa TIMES percepatan
DISPLAY resultanGaya
*/
// Insert your code here
var massa;
var percepatan;
var massa = 600;
var percepatan = 2;
var resultanGaya = massa * percepatan;

console.log(resultanGaya); 

// Insert your code here
var massa;
var percepatan;
var resultanGaya;

/*
//ALGORITMA
1. siapkan nilai massa;
2. siapkan nilai percepatan;
3. siapkan nilai resultanGaya sebagai penampung hasil perkalian;
4. mengalikan nilai massa dan nilai percepatan;
5. menampung hasil nomor 4 di penampung yang telah dipersiapkan di nomor 3;
6. menampilkan hasil perkalian sebagai resultanGaya;
//PSEUDOCODE
STORE massa AS NUMBER WITH 600
STORE percepatan AS NUMBER WITH 2
STORE resultanGaya AS NUMBER WITH ANY VALUE
SET resultanGaya WITH massa TIMES percepatan
DISPLAY "Maka, hasil Resultan Gaya di atas adalah : " CONCAT WITH resultanGaya
*/

var nama;
var nilai;
/*
ALGORITMA
=========
1. Siapkan daftarNilaiMahasiswa yang telah dibuat
2. Siapkan nilaiMahasiswa yang telah disusun
3. Siapkan scoreMahasiswa yang sudah terdaftar
4. Lakukan pemeriksaan
    4.1 Jika nilai mahasiswa di atas 79 dan  kurang dari sama dengan 100 ,maka set hasil dengan score 'A', maka set hasil dengan score 'A'
    4.2 Jika nilai mahasiswa di atas 64 dan kurang dari 80, maka set hasil dengan score 'B'
    4.3 Jika nilai mahasiswa di atas 49 dan kurang dari 65, maka set hasil dengan score 'C'
    4.4 Jika nilai mahasiswa di atas 34 dan kurang dari 50, maka set hasil dengan score 'D'
    4.5 Jika nilai mahasiswa di atas sama dengan 0 dan kurang dari 35, maka set hasil dengan score 'E'
    4.6 Jika ada nilai mahasiswa kurang dari 0 dan juga lebih dari 100, maka set 'Nilai Invalid'
5.Tampilkan setelah input scoreMahasiswa dan namaMahasiswa yang ingin ditampilkan
*/

var nama = 'Aca';
var nilai = 0;
let hasil;
// Insert your code here

if(nilai > 79 && nilai <= 100){
    hasil = `nama: ${nama}; score: A`
}else if(nilai > 64 && nilai < 80){
    hasil = `nama: ${nama}; score: B`
}else if(nilai > 49 && nilai < 65){
    hasil = `nama: ${nama}; score: C`
}else if(nilai > 34 && nilai < 50){
    hasil = `nama: ${nama}; score: D`
}else if(nilai >= 0 && nilai < 35){
    hasil = `nama: ${nama}; score: E`
}else{
    hasil = 'Nilai Invalid'
}
console.log(hasil); 