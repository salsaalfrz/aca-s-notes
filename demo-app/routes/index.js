const express = require('express')
const router = express.Router()
const bannerRouter = require('./banners')
const cartRouter = require('./carts')
const productRouter = require('./products')
const userRouter = require('./users')
const { authentication } = require('../middlewares/auth')

router.use('/', userRouter)
router.use(authentication)
router.use('/products', productRouter)
router.use('/banners', bannerRouter)
router.use('/carts', cartRouter)

module.exports = router