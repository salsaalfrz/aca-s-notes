// ! require supertest
const request = require('supertest')
// ! require app -> untuk menjalankan tester
const app = ('../app')

//! describe -> menjelaskan testing yg berjalan dipakai untuk login
describe ('POST /login must be working', function() {
  it ('should be response with 200 status code', function (done) {
    let body =  {
      email : 'admin@mail.com',
      password: '1234'
    }

    // ! req app 
    request(app)
    // ! endpoint
    .post('/login')
    .send(body)
    // ! mengakhiri sebuah testing
    .end(function(err,res) {
      // ! error -> testing selesai
      if (err) done(err)
      // ! not err
      expect(res.statusCode).toBe(200);
      expect(typeof res.body).toEqual('object')
      expect(res.body).toHaveProperty('access_token')
      expect(res.body).toEqual({
        access_token: expect.any(String)
      })

      done()
    })
  })

  // it ('should response with 400 status code when password and email is null', function(done) {
  //   let body = {
  //     email: '',
  //     password: ''
  //   }

  //   request(app)
  //     .post('/login')
  //     .send(body)
  //     .end(function(err, res) {
  //       if (err) done(err)

  //       expect(res.statusCode).toEqual(404)
  //       expect(typeof res.body).toEqual('object')
  //       expect(res.body).toHaveProperty('message')
  //       expect(res.body.message).toEqual('Password or Email is not valid')

  //       done()
  //     })
  // })

  // it ('should response with 400 status code when email is not found in database', function(done) {
  //   let body = {
  //     email: 'admin@mail.com',
  //     password: '1234'
  //   }

  //   request(app)
  //     .post('/login')
  //     .send(body)
  //     .end(function(err, res) {
  //       if (err) done(err)

  //       expect(res.statusCode).toEqual(404)
  //       expect(typeof res.body).toEqual('object')
  //       expect(res.body).toHaveProperty('message')
  //       expect(res.body.message).toEqual('Password or Email is not valid')

  //       done()
  //     })
  // })

  // it ('should response with 400 status code when password incorrect', function(done) {
  //   let body = {
  //     email: 'admin@mail.com',
  //     password: 'qwert'
  //   }

  //   request(app)
  //     .post('/login')
  //     .send(body)
  //     .end(function(err, res) {
  //       if (err) done(err)

  //       expect(res.statusCode).toEqual(404)
  //       expect(typeof res.body).toEqual('object')
  //       expect(res.body).toHaveProperty('message')
  //       expect(res.body.message).toEqual('Password or Email is not valid')

  //       done()
  //     })
  // })

  //   // ! incorrect pass
  //   it(`should be response with 400 status code when pass is incorrect`,(done) => {
  //     const body = {
  //       email : 'admin@mail.com',
  //       password : 'qweqwe'
  //     }
  
  //   request(app)
  //     .post('/login')
  //     .send(body)
  //     .end((err,res) => {
  //       if (err) done(err)
  //       else {
  //         expect(res.status).toEqual(400)
  //         expect(typeof res.body).toEqual('object')
  //         expect(res.body).toHaveProperty('msg','Invalid email/password')
  //         done()
  //       }
  //     })
  //   })
})
