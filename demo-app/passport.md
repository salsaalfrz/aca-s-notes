1. npm i passport-google-oauth
2. memakai oAuth 2.0 untuk google passport
3. https://www.passportjs.org/docs/google/
4. masuk ke https://console.cloud.google.com/getting-started  untuk configurasi cloud 
5. create project
6. ini nama project dan no organization
7. APIs & Services -> click kanan untuk OAuth consent screen
8. user type -> external
9. app info diisi 
10. developer contact diisi
11. add 3 scopes -> email, profile dan openid
12. save and continue
13. add users -> gmail kalian (save)
14. klik credentials 
15. create -> app type -> web app
16. javascript origins -> uri diisi localhost dan yg sudah di deploy ke dalam heroku
17.localhost/auth/google/callback
18. create
19. masukkin ke dalam .env
20. example : GOOGLE_CLIENT_ID = dsnjfhbcnwlimqi3jg
21. copy configuration ke dalam middleware auth 
22. GOOGLE_CALLBACK_URL dll dimasukkan ke dalam env
23. copy untuk menambahkan router untuk google passport
24. 