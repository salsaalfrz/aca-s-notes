const { User } = require('../models');
const { comparePassword } = require('../helpers/bcrypt')
const { generateToken } = require('../helpers/jwt')
//! require library oauth google
const {OAuth2Client} = require('google-auth-library');

class UserController {
  static register (req, res, next) {
    const { email, password, role} = req.body
    const newUser = { email, password, role}

    User.create(newUser)
      .then((user) => {
        return res.status(201).json({msg: `Created account!`, user})
      })
      .catch((err) => {
        console.log(err);
        next(err)
      })
  }

  static login (req, res, next) {
    const { email, password } = req.body

    // ! if email and password is wrong
    if (!email || !password) {
      next({name: 'InvalidPassOrEmail'})
    }

    User.findOne({ where: {email} })
      .then((user) => {
        if (!user) {
          next({ name: 'InvalidPassOrEmail' })
        } else {
          const isMatch = comparePassword(password, user.password)
          if (!isMatch) {
            next({ name: 'InvalidPassOrEmail' })
          } else {
            const payload = { id: user.id, email: user.email }
            const access_token = generateToken(payload)
            res.status(200).json({ access_token })
          }
        }
      })

      .catch((err) => {
        console.log(err);
        next(err)
      })
  }

  static googleLogin (req,res,next) {
    //! get the token id from body
    const id_token = req.body.id_token
    //! menampung email yg mau dibuat 
    let email;
    const client = new OAuth2Client(process.env.CLIENT_ID);
    client.verifyIdToken({
        //* change the idToken with id_token that we've been defined
        idToken: id_token,
        audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    })

    // ! tiket -> google 
    .then((ticket) => {
      //! untuk dapetin payload dari ticket google
      const payload = ticket.getPayload()
      console.log(payload, `<<< ini tiket dari payload`);

      // ? checking user uda sama belom
      email = payload.email
      return User.findOne({where: {email}})
    })

    // ! checking user udah ada email yg terdaftar?
    .then((user) => {
      // ! if doesnt have an account -> created
      if (!user) {
        return User.create({
          email: email,
          password: Math.random() * 2022 + `bzjkgjcwrhqnqcuyrck1`
        })
      }
      else {
        // ! have an account
        return user
      }
    })

      // ! sending the ticket and token
      .then((user) => {
        const payload = {
          id: user.id,
          email: user.email
      }

      //! sending the token
      const access_token = generateToken(payload)
      res.status(200).json({ access_token })
  })

    .catch((err) => {
        next(err)
    })
  }
}

module.exports = UserController;