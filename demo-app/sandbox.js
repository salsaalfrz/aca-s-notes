
var passport = require('passport');
var GoogleStrategy = require('passport-google-oidc');

// ! router
router.get(
  '/google',
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile'
    ]
  })
)

router.get(
  '/google/callback',
  passport.authenticate('google', { failureRedirect: '/login' }),
  function (req,res) {
    res.redirect('/')
  }
)

module.exports = router;


passport.use(new GoogleStrategy({
    clientID: process.env['GOOGLE_CLIENT_ID'],
    clientSecret: process.env['GOOGLE_CLIENT_SECRET'],
    callbackURL: 'https://www.example.com/oauth2/redirect/google'
  },
  async function (access_token, refreshToken, profile, done) {
    try {
      console.log(profile);
      let userGoogle = await User.findOrCreate({ where: { email: profile.emails[0].value },
        defaults : {
          name: profile.displayName,
          role: 'user'
        }
        })
        return done(null, userGoogle)
    } catch (err) {
      return done(null, false, {
        message: "You're not authorized"
      })
    }

    
    // User.findOrCreate({ googleId: profile.id }, function (err, user) {
    //   return done(err, user);
    // })
  }
))