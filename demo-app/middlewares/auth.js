const { User } = require('../models')
const { verifyToken } = require('../helpers/jwt')

async function authentication (req, res, next) {
  try {
    const decoded = verifyToken(req.headers.access_token)
    const user = await User.findOne({ where : {email : decoded.email}})

    //? if user doesnt exist -> access would be denied
    if (!user) {
      // ! next to error handler which is name of handling error is access denied
      next({name: 'accessDenied'})
    }

  // ? if user exist 
    req.user = { id: user.id, email: user.email, role: user.role}
    next()

  } catch (err) {
    console.log(err);
    // ! next into error handler
    next(err)
  }
}


async function authorization(req, res, next) {
  if (req.user.role === "admin") {
    next()
  }
  else {
    next({ name: "accessDenied"})
  }
}


module.exports = {
  authentication,
  authorization
}