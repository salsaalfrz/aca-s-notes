const bcrypt = require('bcrypt');


const hashPassword = password => {
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync(password, salt)
  return hash
}

const comparePassword = (password,hash) => {
  const compare = bcrypt.compareSync(password,hash)
  return compare
}

module.exports = {
  hashPassword,
  comparePassword
}