'use strict';
const { hashPassword } = require('../helpers/bcrypt')
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsToMany(models.Product, {
        through: models.Cart
      })
    }
  }
  User.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail : {
          args: true,
          msg: `Must be in a email format!`
        },
        notEmpty: {
          args: true,
          msg: `Email is required!`
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args : [4,10],
          msg: `Password must be at least 4 to 10 characters!`
        },
        notEmpty: {
          args: true,
          msg: `Password is required!`
        }
      }
    },
    role: {
      type: DataTypes.STRING
    },
  }, {
    sequelize,
    modelName: 'User',
    hooks: {
      beforeCreate: (instance, options) => {
        instance.password = hashPassword(instance.password)
        instance.role = 'customer'
      }
    }
  });
  return User;
};