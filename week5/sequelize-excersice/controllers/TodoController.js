const { Todo, User } = require('../models')

//! const Todo = rqr('../models/todo)
class TodoController {
  static postTodo (req,res) {
    //! body -> entitas
    const {title, description, status} = req.body
    Todo.create({
      title,
      description,
      status,
      // ? sebagai id user -> foreign key 
      UserId: req.user.id
    })

    .then((data) => {
      res.status(201).json({ msg:
        `todo successfully created`,data
      })
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({msg: `internal server is an error!`})
    })
  }

  static getTodo (req,res) {
    Todo.findAll({
      include : {
          model : User,
          attributes : ["id","email","name"]
      }
    })
    .then((data) => {
      if (!data) {
        res.status(500).json({msg: `Datanya gaada!!`})
      }
      else {
        res.status(200).json({msg: data})
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({msg: `internal server is an error!`})
    })
  }
    static findOne (req,res) {
      const id = req.params.id
      Task.findByPk(id) 
      .then ((data) => {
          res.status(200).json(data)
      })

      .catch ((err) => {
          console.log(err);
          res.status(500).json(err)
      })
  }

  static editStatus(req,res) {
    console.log(`masuk edit`);
    const id = +req.params.id
    const { status } = req.body
    
    Todo.update(
      {status}, 
      {where : {id},
      returning : true})

    .then((data) => {
      console.log(data);
      res.status(200).json(data[1])
    })

    .catch((err) => {
      console.log(err,`<<< masuk err`);
      res.status(500).json(err)
    })
  }

  static putTodo (req,res) {
    const id = req.params.id;
    const {title,description,status,} = req.body;

    Todo.update({
      title,
      description,
      status},
      {where: {id},
      returning: true
    })

    .then((data) => {
      res.status(200).json(data[1],{msg: `Successfully edited task id: ${id}`})
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({msg: `internal server is an error!`})
    })
  }

  static deleteTodo (req,res) {
    const id = +req.params.id
    //! memaksa tipe data berubah ke number (shorthand)

    Todo.destroy({
      where: {id}
    })

    .then((data) => {
      if (!data) {
        res.status(404).json({msg: `Todo is not found`})
      }
      else {
        res.status(200).json({msg: `Todo has been deleted id: ${id}`})
      }
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({msg: `internal server is an error!`})
    })
  }
}

module.exports = TodoController;