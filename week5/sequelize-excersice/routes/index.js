const express = require('express')
const router = express.Router()
const TodoRoute = require('./TodoRoute');
const UserRoute = require('./UserRoute');
const { authenticate } = require('../middlewares/auth')

router.use('/user', UserRoute);
router.use(authenticate)
router.use('/todo',TodoRoute);

module.exports = router;