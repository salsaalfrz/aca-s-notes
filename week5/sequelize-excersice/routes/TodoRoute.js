//! panggil controller 

const express = require('express')
const router = express.Router()
const TodoController = require('../controllers/TodoController')
//! authorize
const { authorize } = require('../middlewares/auth')

//! endpoint
router.post('/', TodoController.postTodo);
router.get('/', TodoController.getTodo);
router.put('/:id', TodoController.putTodo);
router.patch('/:id', TodoController.editStatus);
router.delete('/:id',authorize, TodoController.deleteTodo);



module.exports = router;