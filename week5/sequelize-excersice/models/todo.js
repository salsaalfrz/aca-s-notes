'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Todo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //! berhubungan dengan user yg memiliki foreignKey userid
      Todo.belongsTo(models.User, {foreignkey:"UserId"})
    }
  };
  Todo.init({
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `title must be filled!`
        }
      }
    },
    description: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `desc must be filled!`
        }
      }
    } ,
    status:{
      type: DataTypes.BOOLEAN,
    } 
  }, {
    sequelize,
    modelName: 'Todo',
  });
  return Todo;
};