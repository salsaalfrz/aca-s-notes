// ! Algoritma -> solusi untuk menyelesaikan suatu masalah (di implementasikan kedlm program komputer)
// * - step by step untuk menyelesaikan problem di pemrograman komputer
/**
 * * Kenapa harus ada algoritman?
 * 1. Karena adanya suatu masalah 
 * 2. Membuat algoritma untuk pedoman menyelesaikan problem
 * 3. Implementasi ke dlm perangkat
 * 4. solusi terpecahkan
 */

/**
 * ! conclusion
 * 1. menyusun proses logika dari pemecahan masalah 
 * 2. urutan logic langkah kerja untuk mnyelesaikan suatu masalah
 */

// ! Notasi Algoritma
/**
 * * Kalimat deskriptif -> menuliskan instruksi yg harus dilakukan dg bahasa yg jelas
 * * Flowchart -> menuliskan step by step yg harus dilakuka dlm bentuk simbol
 * * Pseudocode -> step by step menggunakan code yg tidak terikat pada bahasa pemrograman tertentu
 */

/**
 * example :
 * ! cara memasak indomie + telor
 * 1. siapkan indomie
 * 2. rebus air
 * 3. buka indomie
 * 4. masukkan indomie
 * 5. siapkan piring
 * 5. tuang bumbu ke piring
 * 6. tunggu mie smpai matang
 * 7. setelah matang maka tiriskan 
 * 8. tuang dulu ke piring
 * 9. aduk hingga merata
 * 10. siapkan garpu lalu siap dihidangkan 
 * 
 */

// ! merancang flowchart tidak ada hal yg pasti (mutlak). karena didasar oleh flowchart yaitu gambaran dari hasil pemikiran dlm menganalisa suatu permasalahan dlm komputer

/**
 * ! program menghitung luas lingkaran
 * rumus = L = phi. r^2
 * 1. identifikasi radius/jari-jari
 * 2. identifikasi phi = 3.14 atau 22/7
 * 3. siap kan rumus nya 
 * 4. bikin variabel untuk menyimpan sebuah radius atau phi
 */

/**
 * PSEUDOCODE
 * STORE phi AS Number WITH 3.14
 * STORE radius AS Number WITH 14
 * STORE multiply AS Number WITH ANY VALUE
 * 
 * SET Multiply WITH phi TIMES radius TIMES radius
 * DIPLAY multiply
 */

const phi = 3.14;
let radius = 14;
let multiply;
console.log(multiply);

let sum = phi * radius ** 2 //kuadrat 
console.log(sum);

let kuadrat = 11 ** 2;
console.log(kuadrat);
/**
 * ! SOAL
 * Bunyi hukum II Newton adalah:

Percepatan sebuah benda akan sebanding dengan gaya yang diberikan pada benda dan berbanding terbalik dengan massa benda. Arah percepatan benda sama dengan arah gaya total yang diberikan pada benda.
Secara matematis hukum II Newton dirumuskan sebagai berikut: ΣF = m x a

ΣF = resultan gaya (Newton)

m = massa benda (kg)

a = percepatan benda (m/s2)

Berdasarkan keterangan di atas, buatlah sebuah algoritma dan pseudocode untuk menghitung resultan gaya pada sebuah mobil yang memiliki massa benda 600 kg dan ketika didorong oleh tiga orang percepatannya adalah 2 m/s2!
 */


/**
 * PSEUDOCODE 
 * ==========
 * STORE resultanGaya AS Number WITH ANY VALUE
 * STORE massaBenda AS Number WITH 600
 * STORE percepatanBenda AS Number WITH 2
 * 
 * SET resultanGaya WITH resultanGaya MULTIPLY massaBenda
 * DIPLAY resultanGaya
 */

let resultanGaya;
let massaBenda = 600;
let percepatanBenda = 2;

resultanGaya = massaBenda * percepatanBenda;

console.log(resultanGaya);

/**
 * Ada seorang pengunjung coffee shop yang mempunyai biodata name,age,money.
 * Ia masuk untuk memesan espresso, namun untuk memesan espresso harus memiliki kondisi sebagai berikut:
 * 
 * - Jika name dari pengunjung tidak kosong, lanjut ke step ke 2. kalau gaada gabole masuk "Ada tidak boleh masuk!"
 * 
 * - Jika age dari si pengunjung dibawah 18 tahun, maka hanya boleh memesan Ice Chocolate. 
 * - Jika age 18 tahun keatas, ia hanya boleh memesan espresso.
 * 
 * - Ice Chocolate memiliki harga 45000, sedangkan Espresso memiliki harga 100000.
 * 
 * - Jika money yang dimiliki tidak mencukupi, maka tampilkan "Your money is'nt enough. You must go home."
 * 
 * - Jika money cukup, tampilkan "You can order. Change : [..]"
 * dan ganti [..] dengan sisa uang yang telah dikurang oleh harga minuman 
 * 
 */

/**
 * PSEUDOCODE
 * ==========
 * 
 * STORE name AS string WITH ANY VALUE
 * STORE age AS number WITH ANY VALUE
 * STORE money AS number WITH ANY VALUE
 * STORE result AS undefined WITH ANY VALUE
 * 
 * IF name IS FALSY
 *    SET result AS "Ada tidak boleh masuk!"
 * ELSE 
 *    IF age IS LESS THAN 18
 *      IF  money IS LESS THAN 45000
 *        SET result "Your money is'nt enough. You must go home."
 *      ELSE 
 *        money MINUS AND EQUALS TO 45000
 *        SET result `You can order. Change : CONCAT WITH  ${money}`
 *      END IF
 *    END IF 
 * END IF
 */