/**
 * Ada seorang pengunjung coffee shop yang mempunyai biodata name,age,money.
 * Ia masuk untuk memesan espresso, namun untuk memesan espresso harus memiliki kondisi sebagai berikut:
 * 
 * - Jika name dari pengunjung tidak kosong, lanjut ke step ke 2. kalau gaada gabole masuk "Ada tidak boleh masuk!"
 * 
 * - Jika age dari si pengunjung dibawah 18 tahun, maka hanya boleh memesan Ice Chocolate. 
 * - Jika age 18 tahun keatas, ia hanya boleh memesan espresso.
 * 
 * - Ice Chocolate memiliki harga 45000, sedangkan Espresso memiliki harga 100000.
 * 
 * - Jika money yang dimiliki tidak mencukupi, maka tampilkan "Your money is'nt enough. You must go home."
 * 
 * - Jika money cukup, tampilkan "You can order. Change : [..]"
 * dan ganti [..] dengan sisa uang yang telah dikurang oleh harga minuman 
 * 
 */

/**
 * 1. bikin variabel yang telah ditentukan
 * 2.  unique case -> nama kosong 
 */

let name = "Icing";
let age = 24;
let money = 200000;

// ! ketika kita memakai const -> kita gabisa merubahnya. 

//* truthy -> true, 1 (kebalikan dari si falsy) 
// * falsy -> false, NaN, null, undefined, "", -0, 0, 0n. -> (symbol -> !)
// if (!name) { //ketika pengunjung tidak ada nama 
//   console.log('Anda tidak boleh masuk!');
// } else {
//   // * pengecekan umur dibawah 18 tahun 
//   if (age < 18) {
//     // ! check money cukup atau tidak ?
//     if ((money - 45000) < 0) {
//       // ! uangnya gacukup
//       // ! kalau < 0 otomatis minus 
//       console.log('Your money inst enough. Pulang')
//     } else {
//       // !uangnya cukup > 0 
//       console.log("You can order, CHange: " + (money-45000));
//     }
//   } else {
//     // * pengecekan 18 tahun keatas 
//     if ((money - 100000) < 0) {
//       console.log('PUlang sana');
//     } else {
//       console.log("You can order, Change: " + (money - 100000));
//     }
//   }
// }

let result; 

console.log(typeof result);

if (!name) {
  result = "Ada tidak boleh masuk!"
} else {
  // cek umur 
  // 18 ke bawah
  if (age < 18) {
    if (money < 45000) {
      result = "Your money is'nt enough. You must go home.";
    } else {
      money -= 45000;
      result = `You can order. Change : ${money}`
    }

  } else {
    if (money < 100000) {
      result = "Your money is'nt enough. You must go home.";
    } else {
      money -= 100000;
      result = `You can order. Change : ${money}`
    }
  }
}
console.log(result);

if (name) {
  if (age < 18) {
    if (money < 45000) {
      result = "Your money is'nt enough. You must go home.";
    } else {
      money -= 45000;
      result = `You can order. Change : ${money}`
    }

  } else {
    if (money < 100000) {
      result = "Your money is'nt enough. You must go home.";
    } else {
      money -= 100000;
      result = `You can order. Change : ${money}`
    }
  }
} else {
  result = "Ada tidak boleh masuk!";
}

console.log(result);

console.log(typeof result);

// let number = "123";
// console.log(number);
// console.log(Number(number));
//! Number() -> method untuk merubah data type sebelumnya menjadi number 

// let num = "17"
// console.log(isNaN("17")); //false
// ! jika mengecek isNaN berupa string yang berbentuk angka maka di convert terlebih dahulu menjadi number. 
// ! isNaN akan convert ke number terlebih dahulu 
// console.log(isNaN(19)); //false
// // ! isNaN -> is not a number

// ? isNaN ->convert Number() -> false
// ? isNaN -> string -> convert Number() -> akan tetap menjadi string -> true 
// console.log(isNaN("acil17")); //true 

