/**
 * Buatlah algoritma dan implementasi di javascript untuk kasus berikut: Seorang pengajar sedang memeriksa ujian mahasiswa dan akan memberikan desc nilai dari A-E dengan ketentuan sebagai berikut:

Nilai 80 - 100: A
Nilai 65 - 79: B
Nilai 50 - 64: C
Nilai 35 - 49: D
Nilai 0 - 34: E Tampilkan desc nilai dan nama siswa saat pengajar tersebut memasukkan nilai dan nama yang dia inginkan.
NOTED: Jika nilai mahasiswa kurang dari 0 atau lebih dari 100 maka tampilkan 'Nilai Invalid'


Diberikan 3 variable yang me-representasikan seorang murid:

nama
nilai
absen
Buatlah algoritma/pseudocode dan implementasi sebuah kondisional yang menentukan apakah murid tersebut lulus atau tidak.

Murid dinyatakan lulus jika:

nilainya 70 keatas
absennya dibawah 5 kali
Tampilkan nama murid dan keterangan apakah murid tersebut 'lulus' atau 'tidak lulus'

RESTRICTION: Tidak boleh menggunakan built-in function apapun
 */